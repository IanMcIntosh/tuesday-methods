﻿using System;

namespace tuesday_methods
{
    class Program
    {
        static void Main(string[] args)
        {
        FirstMethod();
        FirstMethod();
        FirstMethod();
        FirstMethod();
        FirstMethod();
        SecondMethod("Ian");
        SecondMethod("Luke");
        SecondMethod("Bobby");
        SecondMethod("Jack");
        SecondMethod("Mark");
        Console.WriteLine(ThirdMethod("Jill"));
        
        }

        static void FirstMethod()
        {
        Console.WriteLine("My First Method");
        }

        static void SecondMethod(string name)
        {
        Console.WriteLine($"Hello {name}");
        }

        static string ThirdMethod(string name)
        {
            return $"Hello {name}";
        }
    }
}
